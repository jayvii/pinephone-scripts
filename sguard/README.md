# Suspend Guard for Phosh

This project is REUSE compliant: https://reuse.software

## What does it do?

Prevent auto-suspend as long as media (audio) is playing, e.g. from your
podcast- or music-player.

Some players (like `lollypop`) have built-in suspend-inhibit, but not all of
them have yet (e.g. `gnome-podcasts`) and some probably never will
(e.g. `firefox`).

## How does it work?

The `sguard.sh` script checks every X seconds (default: 60), whether sound is
playing through any pulse-audio channel. If it is, it will inhibit suspend for
the next X+1 seconds, until it checks again.

## Are there system requirements?

Yes, the script uses `pactl`, which is probably installed anyways and
`gnome-session-inhibit` which is part of the GNOME desktop and therefore part of
phosh.

## Wouldn't it be better to check mpris for the current player state?

Indeed, that would be a _cleaner_ approach, and the [script did exactly that for
a while](https://src.jayvii.de/pub/pinephone-scripts/commit/ea05e205a1315464bb9ebda0ad86493eb844e395.html).
However, it turns out that phosh handles MediaPlayer/mpris events in an odd way,
such that it does not report any playing state, when the screen is locked.

Additionally, some applications do not support mpris and would therefore be
unable to trigger the suspend-inhibit.

## Use sguard

Just download this source directory, and run the Makefile:
```
git clone https://src.jayvii.de/pub/pinephone-scripts.git
cd pinephone-scripts/sguard
make install
```

This will place `sguard.sh` into `~/.local/bin/sguard.sh` and place the
systemd-service file under your user's directory. You can enable & start sguard
with:
```
systemctl --user enable sguard --now
```

## Maintainer

- JayVii <jayvii [AT] posteo [DOT] de>

## Contribute to sguard

Simply send a mail to one of the Maintainers, describing your issue(s) and ideally
proposing a solution as well :)

### Contributors

- JayVii <jayvii [AT] posteo [DOT] de> (2021)
- HazardChem <pthom44 [AT] live [DOT] com [DOT] au> (2021)
