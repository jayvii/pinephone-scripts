# PinePhoneScripts

Just a collection of (hopefully) useful scripts for the PinePhone (and
potentially other devices).

Right now, there is only "sguard", please refer to its README to find out more:
https://src.jayvii.de/pub/pinephone-scripts/file/sguard/README.md.html
